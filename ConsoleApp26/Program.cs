﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp26
{
    class Program
    {
        static void Main(string[] args)
        {
            NorthwindEntities ne = new NorthwindEntities();

            string herodesFile = @"..\..\Herodes.jpg";
            var buff = File.ReadAllBytes(herodesFile);
            ne.Employees.Add(new Employee
            {
                FirstName = "Herodes",
                LastName = "Great",
                Title = "the King",
                HireDate = DateTime.Now,
                Photo = buff
            });
            ne.SaveChanges();

            ne.Employees
                .OrderBy(x => x.EmployeeID)
                //.Skip(8)
                .ToList()
                .ForEach(
                x =>
                {
                    Bitmap b = new Bitmap(new MemoryStream(x.Photo.Skip(x.EmployeeID < 10 ? 78 : 0).ToArray()));
                    Form1 f = new Form1();
                    f.pictureBox1.Image = b;
                    f.ShowDialog();
                }
                );
        }
    }
}
